const express = require('express');
const cors = require('cors');
const http = require('http');
const app = express();
const server = http.createServer(app);

// Settings
const port = process.env.PORT || 4000;

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

// Routes
app.use('', require('./routes/register.routes'));
app.use('', require('./routes/registerCredito.routes'));


// Iniciar Server
server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${port}`);
});