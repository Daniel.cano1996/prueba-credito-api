const mysql = require('mysql');
const { promisify } = require('util');

const { database } = require('./key')


// Conexión a mysql
const pool = mysql.createPool(database);
// const connection = mysql.createConnection(database)

// Validar la conexión a mysql y la detección de errores
pool.getConnection((err, connection) => {
    if(err) {
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('La conexión con la base de datos fué cerrada');
        }

        if(err.code === 'ER_CON_COUNT_ERROR') {
            console.error('La base de datos tiene muchas conexiones');
        }

        if(err.code == 'ECONNREFUSED') {
            console.error('La conexión a sido rechazada');
        }
    }

    if(connection){
        console.log('Base de datos conectada');
        return connection.release()
    } else {
        console.error('Error, verifique los datos de conexión de la BBDD');
    }

});


// Permite usar async/await o promesas en las consultas
pool.query = promisify(pool.query);

module.exports = pool;