const express = require('express');
const router = express.Router();

const CLRregister = require('../controllers/register.controller');


router.post('/register', CLRregister.register);
router.get('/findAll', CLRregister.find)


module.exports = router;