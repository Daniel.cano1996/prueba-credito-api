const db = require('../database');
const ctrlRegister = {}

ctrlRegister.register = async (req, res) => {


    var newClient = {
        identificacion: req.body.Identificacion,
        nombre: req.body.Nombre,
        apellido: req.body.Apellido,
        fecha_Nacimiento: req.body.Fecha_Nacimiento,
        username: req.body.Username,
        pass: req.body.Password
    }

    console.log(newClient);

       // Consultar si la identificacion que ingresó en el form se encuentra en BBDD
    var identificacion_cliente = newClient.identificacion;
    await db.query('SELECT * FROM cliente WHERE identificacion=?', [identificacion_cliente], async (err, rows, fields) => {

        try {
            if (rows[0]) {
                
                res.json({
                    mensaje: 'ya esta en la base de datos'
                })
    

            } else {
                await db.query('INSERT INTO cliente set ?', [newClient]);

                res.json({
                    mensaje: 'cliente creado ',
                    result: newClient,
                });

            }
        } catch (error) {
            // res.json(err)
            console.log('Error', error);
        }
    })

}

ctrlRegister.find = async (req, res) => {
    await db.query('SELECT * FROM cliente', async (err, rows, fields) => {
        console.log(rows);
        try {
            if (!rows) {
                res.json({
                    mensaje: 'No hay registros de clientes'
                });
            } else {
                res.json({
                    mensaje: 'ok',
                    result: rows
                });
            }
        } catch (err) {
            console.log(err);
        }
    });
}


module.exports = ctrlRegister;