const db = require('../database');
const ctrlRegisterCredito = {}

ctrlRegisterCredito.registerCredito = async (req, res) => {
    console.log(req.body.id_Cliente);

    var newCredito = {
        nombre_Empresa: req.body.Nombre_Empresa,
        nit_Empresa: req.body.Nit,
        salario_Actual: req.body.Salario,
        fecha_Ingreso: req.body.Fecha_Empresa_Ingreso,
        id_Cliente: req.body.idCliente
    }

    console.log(newCredito);

       // Consultar si el Nit que ingresó en el form se encuentra en BBDD
    var identificacion_empresa = newCredito.nit_Empresa;
    await db.query('SELECT * FROM aprobacion_credito WHERE nit_Empresa=?', [identificacion_empresa], async (err, rows, fields) => {

        try {
            if (rows[0]) {
                
                res.json({
                    mensaje: 'ya esta en la base de datos'
                })
    

            } else {
                await db.query('INSERT INTO aprobacion_credito set ?', [newCredito]);

                res.json({
                    mensaje: 'Puede acceder al crédito.',
                    result: newCredito,
                });

            }
        } catch (error) {
            // res.json(err)
            console.log('Error', error);
        }
    })

}


module.exports = ctrlRegisterCredito;